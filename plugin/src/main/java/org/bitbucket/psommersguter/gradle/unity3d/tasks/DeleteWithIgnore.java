/************************************************************
 *  Copyright (c) 2015 Philipp Sommersguter                 *
 *                                                          *
 *  See the file LICENCE.txt for copying permission.        *
 ***********************************************************/
package org.bitbucket.psommersguter.gradle.unity3d.tasks;

import org.gradle.api.tasks.Delete;
import org.gradle.api.tasks.TaskExecutionException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * <b>A delete task that can ignore content</b>
 */
public class DeleteWithIgnore extends Delete {

    /**
     * The relative path (originating prom project root) of the directory to clean
     */
    private String targetDirectory;

    /**
     * A list of files to ignore during clean
     */
    private String[] filesToIgnore;

    @Override
    protected void clean() {
        configureClean();
        super.clean();
    }

    /**
     * Creates a list of all the files to delete in the {@link #targetDirectory}
     * while ignoring the ones specified by {@link #filesToIgnore}.
     */
    private void configureClean() {
        try {
            Path projectLocationPath = Paths.get(targetDirectory);

            List<File> filesToClean = new ArrayList<>();

            Files.walk(projectLocationPath).filter(
                    pathToFilter -> filterFilesToClean(projectLocationPath, pathToFilter)
            ).forEach(
                    path -> filesToClean.add(path.toFile())
            );

            delete(filesToClean);
        } catch (IOException e) {
            throw new TaskExecutionException(this, e);
        }
    }

    /**
     * Decides whether the path should be deleted.
     * @param projectLocationPath the path of the Unity3d engine project
     * @param currentPath the path of the file to be filtered
     * @return True if the file should be deleted, false otherwise
     */
    private boolean filterFilesToClean(Path projectLocationPath, Path currentPath){
        //ignores the root folder, otherwise everything is deleted
        if(currentPath.equals(projectLocationPath)){
            return false;
        }

        //ignores the files which are on the ignore list
        for(String fileToIgnore : filesToIgnore){
            Path pathToIgnore = Paths.get(fileToIgnore);
            if(projectLocationPath.relativize(currentPath).startsWith(pathToIgnore)){
                return false;
            }
        }
        return true;
    }

    public void setTargetDirectory(String targetDirectory) {
        this.targetDirectory = targetDirectory;
    }

    /**
     * Specify all files or directories to ignore during clean of the target directory.
     * @param filesToIgnore Specify relative to the {@link DeleteWithIgnore#targetDirectory}
     */
    public void ignore(String... filesToIgnore){
        this.filesToIgnore = filesToIgnore;
    }
}

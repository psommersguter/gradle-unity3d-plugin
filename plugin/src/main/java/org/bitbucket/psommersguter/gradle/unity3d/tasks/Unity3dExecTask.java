/************************************************************
 *  Copyright (c) 2015 Philipp Sommersguter                 *
 *                                                          *
 *  See the file LICENCE.txt for copying permission.        *
 ***********************************************************/
package org.bitbucket.psommersguter.gradle.unity3d.tasks;

import org.bitbucket.psommersguter.gradle.unity3d.Unity3dPlugin;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

/**
 * <b>A build task that executes a specified method and optionally some additional build arguments</b>
 */
public class Unity3dExecTask extends AbstractUnity3dExecTask {

    public static final String UNITY_INVOKE_URL = "http://localhost:9173/unity/executemethod/";
    /**
     * A list of arguments to use additionally to the ones returned by {@link #getDefaultArgs()}
     */
    private List<String> buildArgs;

    public Unity3dExecTask() {
        super(Unity3dExecTask.class);
    }

    /**
     * Sets the correct arguments for the execution of the task
     */
    private void configureBuildArgs() {
        List<String> execArgs = getDefaultArgs();

        if(buildArgs != null && buildArgs.size() > 0) {
            execArgs.addAll(buildArgs);
        }

        if(getLogger().isDebugEnabled()){
            execArgs.forEach(s -> getLogger().debug("execArg = "+ s));
        }

        //noinspection unchecked
        args(execArgs);
    }

    @Override
    protected void exec() {
        if(!performInvokeMethod()) {
            configureBuildArgs();
            super.exec();
        }
    }

    /**
     * When {@link #buildArgs} contains the parameters to execute a
     * method in the Unity editor, it tries to send the method to the running editor.
     * @return true if the method was successfully sent, false otherwise
     */
    private boolean performInvokeMethod() {
        try{
            int paramIndex = buildArgs.indexOf(Unity3dPlugin.PARAM_EXECUTE_METHOD);
            if( paramIndex >= 0) {
                String executeMethodName = buildArgs.get(paramIndex+1);
                return sendToRunningUnity(executeMethodName);
            }
        } catch (IOException ignored) {}
        return false;
    }

    /**
     * Opens a HTTP connection to the endpoint opened by the auxiliary project.
     * @param methodName The full name of the method to call in the editor
     * @return true if the connection sent the HTTP OK response, false otherwise
     * @throws IOException when the connection attempt was not successful
     */
    private boolean sendToRunningUnity(String methodName) throws IOException {
        HttpURLConnection connection = (HttpURLConnection) new URL(UNITY_INVOKE_URL +methodName).openConnection();
        boolean validResponse =  connection.getResponseCode() != 20;
        connection.disconnect();
        return validResponse;
    }

    //region Getter and Setter

//    public void buildArgs(String... buildArgs) {
//        this.buildArgs = new ArrayList<>(Arrays.asList(buildArgs));
//    }

    public void setBuildArgs(List<String> buildArgs) {
        this.buildArgs = buildArgs;
    }

    //endregion
}

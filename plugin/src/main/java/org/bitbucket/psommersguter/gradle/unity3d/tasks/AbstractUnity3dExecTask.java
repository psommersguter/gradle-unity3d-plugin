/************************************************************
 *  Copyright (c) 2015 Philipp Sommersguter                 *
 *                                                          *
 *  See the file LICENCE.txt for copying permission.        *
 ***********************************************************/
package org.bitbucket.psommersguter.gradle.unity3d.tasks;

import groovy.lang.MissingPropertyException;
import org.gradle.api.Task;
import org.gradle.api.logging.LogLevel;
import org.gradle.api.tasks.AbstractExecTask;
import org.jenkinsci.plugins.unity3d.io.PipeFileAfterModificationAction;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * <b>Base class for a exec task to build a project with the Unity3d engine.</b>
 */
public abstract class AbstractUnity3dExecTask extends AbstractExecTask{

    /**
     * The path to the project to build.
     */
    protected String projectPath;
    /**
     * The path to the log file to create while building
     */
    protected String logFile;


    @SuppressWarnings("unchecked")
    public AbstractUnity3dExecTask(Class taskType) {
        super(taskType);
    }

    /**
     * A task action that creates the output folder of the log file. Unity3d apparently cannot do this on their own. <br>
     * This must action must be executed before {@link #exec()}
     * @see org.gradle.api.tasks.TaskAction
     * @param task not used
     */
    public void createOutputDirs(@SuppressWarnings("UnusedParameters") Task task){
        getLogger().info("createOutputDirs()");
        //noinspection ResultOfMethodCallIgnored
        new File(logFile).getParentFile().mkdirs();
    }

    @Override
    protected void exec() {
        pipeUnityLogToConsole();
        super.exec();
    }

    /**
     * Defines the default arguments to use when calling Unity3d to build the project.
     * @return A list of default arguments
     */
    protected List<String> getDefaultArgs() {
        return new ArrayList<>(Arrays.asList(
                "-batchmode", "-quit", "-projectPath", projectPath, "-logFile", logFile
        ));
    }

    /**
     * Calls {@link #startLogFileWatcher()} if logging is enabled or the property <i>pipeUnityLog</i> is defined on the project.
     */
    private void pipeUnityLogToConsole() {
        boolean isLogEnabled = getLogger().isEnabled(LogLevel.INFO);
        boolean pipeLogFilePropertySet = false;
        try {
            pipeLogFilePropertySet = getProject().hasProperty("pipeUnityLog");
        }catch (MissingPropertyException ignored) {}

        if(isLogEnabled || pipeLogFilePropertySet){
            startLogFileWatcher();
        }
    }

    /**
     * Pipes the content of the {@link #logFile} to {@link System#out}.
     */
    private void startLogFileWatcher() {
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        executorService.submit(new PipeFileAfterModificationAction(logFile, this.getStandardOutput()));
    }

    //region Getter and Setter

    public void setLogFile(String logFile) {
        this.logFile = logFile;
    }

    public void setProjectPath(String projectPath) {
        this.projectPath = projectPath;
    }

    //endregion
}

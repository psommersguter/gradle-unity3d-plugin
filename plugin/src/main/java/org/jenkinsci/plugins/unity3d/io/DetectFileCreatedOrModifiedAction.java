/*
    The MIT License

    Copyright (c) 2011-, Jerome Lacoste

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
*/
package org.jenkinsci.plugins.unity3d.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.concurrent.Callable;

/**
 * A Callable that waits until a file has been created or modified.
 * <p>
 * The task checks for changes in either the last modified timestamp (precise up to the second) or the file size to detect a change.
 * <p>
 * The task cannot currently be stopped, and its time between checks is hardcoded (50 msec).
 *
 * @author Jerome Lacoste
 */
public class DetectFileCreatedOrModifiedAction implements Callable<File> {
    private String path;
    private long origLastModified;
    private long origSize;
    private boolean origExists;
    private final int timeoutBetweenChecks;

    public DetectFileCreatedOrModifiedAction(String path) {
        this.path = path;
        File orig = new File(path);
        origExists = orig.exists();
        origLastModified = origExists ? orig.lastModified() : 0;
        origSize = origExists ? orig.length() : -1;
        timeoutBetweenChecks = 50;
    }

    public File call() throws FileNotFoundException {
        while (true) {
            File file = new File(path);
            if (hasChanged(file)) {
                return file;
            }
            synchronized (this) {
                try {
                    wait(timeoutBetweenChecks);
                } catch (InterruptedException e) {
                    return null;
                }
            }
        }
    }

    private boolean hasChanged(File file) {
        if (!origExists) return file.exists();
        else return file.length() != origSize || file.lastModified() > origLastModified;
    }
}

/************************************************************
 *  Copyright (c) 2015 Philipp Sommersguter                 *
 *                                                          *
 *  See the file LICENCE.txt for copying permission.        *
 ***********************************************************/
package org.bitbucket.psommersguter.gradle.unity3d;

import org.gradle.api.logging.Logger;
import org.gradle.api.logging.Logging;
import org.gradle.tooling.BuildException;
import org.gradle.tooling.BuildLauncher;
import org.gradle.tooling.GradleConnector;
import org.gradle.tooling.ProjectConnection;
import org.gradle.tooling.internal.consumer.DefaultGradleConnector;
import org.junit.Before;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.OutputStream;


/**
 * <b>Provides base functionality to use for integration testing.</b>
 */
public class IntegrationTestBase {

    /**
     * The name of a environment variable that contains a path to a Unity3d installation used for running integration tests.
     */
    private static final String ENV_TEST_EDITOR_LOCATION = "UNITY3D_PLUGIN_TEST_UNITY_EDITOR";

    protected static final Logger logger = Logging.getLogger(IntegrationTestBase.class);

    protected DefaultGradleConnector  gradleConnector;

    @Before
    public void before() {
        gradleConnector = (DefaultGradleConnector)GradleConnector.newConnector();
        gradleConnector.embedded(true); //Prevents starting of the gradle daemon
        //The daemon would block the .jar containing the plugin so subsequent "clean" tasks would fail
    }

    protected ProjectConnection openProjectConnection(String testProjectDirectory) {
        gradleConnector.forProjectDirectory(new File(getAsTestProjectPath(testProjectDirectory)));
        return gradleConnector.connect();
    }

    protected String getAsTestProjectPath(String testProjectDirectory) {
        return "build" + File.separator + "resources" + File.separator + "test" + File.separator + testProjectDirectory;
    }

    protected ByteArrayOutputStream launchTasks(ProjectConnection connection, String... taskNames) throws Throwable {
        return launchTasks(connection, null, taskNames);
    }

    protected ByteArrayOutputStream launchTasks(ProjectConnection connection, String[] arguments, String... taskNames) throws Throwable {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        try {
            BuildLauncher launcher = connection.newBuild();
            launcher.setStandardOutput(outputStream);
            launcher.forTasks(taskNames);
            if(arguments != null) {
                launcher.withArguments(arguments);
            }
            launcher.run();
            return outputStream;
        } catch (BuildException ex){
            rethrowDeepestCause(ex.getCause());
        } finally {
            connection.close();
        }
        return null;
    }

    protected String getEditorArgument() {
        final String environmentEditorPath = System.getenv(ENV_TEST_EDITOR_LOCATION);
        return getEditorArgument(environmentEditorPath);
    }

    protected String getEditorArgument(String unityEditorPath) {

        return "-Punity3d.editor=" + unityEditorPath;
    }

    protected String[] asArray(String... values) {
        return values;
    }

    private void rethrowDeepestCause(Throwable e) throws Throwable {
        if(e.getCause() == null){
            throw e;
        } else {
            rethrowDeepestCause(e.getCause());
        }
    }

}

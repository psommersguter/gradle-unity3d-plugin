# gradle-unity3d-plugin #

This is a gradle plugin to allow support for the Unity game engine. (www.unity3d.com)

For general information about the plugin, please visit the [wiki](https://bitbucket.org/psommersguter/gradle-unity3d-plugin/wiki/Home).

# Feedback and Contributions #
Both feedback ([message](https://bitbucket.org/account/notifications/send/?receiver=psommersguter)|[email](mailto:philipp.sommersguter@gmail.com)) and [contributions](https://bitbucket.org/psommersguter/gradle-unity3d-plugin/wiki/How%20To%20Contribute) are very welcome.

# Acknowledgements #

[Jerome Lacoste](https://github.com/lacostej) for creating the [jenkins-unity3d-plugin](https://github.com/lacostej/jenkins-unity3d-plugin) which was an inspiration to start the gradle plugin and provided a way to pipe out the unity log to the console while building.

# Licence #

This plugin is available under the [MIT License](http://opensource.org/licenses/MIT)

Copyright (c) 2015 Philipp Sommersguter
/************************************************************
 *  Copyright (c) 2015 Philipp Sommersguter                 *
 *                                                          *
 *  See the file LICENCE.txt for copying permission.        *
 ***********************************************************/
package org.bitbucket.psommersguter.gradle.unity3d;

import org.bitbucket.psommersguter.gradle.unity3d.convention.Unity3dExtension;
import org.gradle.api.Plugin;
import org.gradle.api.Project;

/**
 * <b>Provides the initialization of the plugin.</b>
 */
public class Unity3dPlugin implements Plugin<Project>{

    /**
     * Unity3d batchmode parameter to define a method to execute.
     */
    public static final String PARAM_EXECUTE_METHOD = "-executeMethod";

    /**
     * <b>A gradle project property to hold a path pointing to the direction of the Unity3d engine's installation directory.</b>
     *
     * <p>
     * The capitalization in the properties value is ignored.
     * Additionally, gradle does not care if you use / or \ as a path separator.<br>
     * Examples for valid inputs on Windows:
     * <ul>
     *     <li>X:\\Path\\To\\Unity</li>
     *     <li>X:/Path/To/Unity/Editor</li>
     *     <li>X:\\Path/To\\Unity/Editor\\Unity.exe <i>(This use of
     *     file separators would actually work! But please, just don't do it)</i></li>
     * </ul>
     * </p>
     */
    public static final String PROPERTY_EDITOR = Unity3dExtension.NAME + ".editor";

    /**
     * Entry point for the plugin. Gets executed at line 'apply plugin: ...' in the build.gradle
     */
    @Override
    public void apply(Project project) {
        if(!project.hasProperty(PROPERTY_EDITOR)) {
            throw new IllegalArgumentException("You must set the location of the Unity editor in " +
                    "a gradle.properties file or via -Punity3d.editor=XYZ");
        }

        Unity3dExtension extension = Unity3dExtension.initialize(project);

        project.afterEvaluate(extension::createDefaultCleanTasks);
        project.afterEvaluate(extension::createBuildTasksFromConventions);
        project.afterEvaluate(extension::createDependencyTasks);
    }


}

﻿using System;
using System.Reflection;
using UnityEditor;

namespace GradleUnity3dPlugin.Auxiliary
{
    [InitializeOnLoad]
    public class CallMethodService
    {
        private const string ASSEMBLY_QUALIFIERS = ", Assembly-CSharp-Editor, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null";

        private static HttpService httpService;
        
        static CallMethodService()
        {
            httpService = new HttpService();
        }

        [InitializeOnLoadMethod]
        public static void OnInitializeEditor() {
            httpService.StartService();
            EditorApplication.update += OnEditorUpdate;
        }

        private static void OnEditorUpdate()
        {
            if (httpService.RequestInvokeTarget != null)
            {
                InvokeTargetMethod(httpService.RequestInvokeTarget);
                httpService.RequestInvokeTarget = null;
            }
        }

        private static void InvokeTargetMethod(string invokeTarget)
        {
            int lastDotIndex = invokeTarget.LastIndexOf(".");
            String typeName = invokeTarget.Substring(0, lastDotIndex);
            String methodName = invokeTarget.Substring(lastDotIndex + 1);

            Type invokeOnType = Type.GetType(typeName + ASSEMBLY_QUALIFIERS);
            invokeOnType.GetMethod(methodName, 
                BindingFlags.Public | BindingFlags.Static | BindingFlags.NonPublic)
                .Invoke(null, null);
        }

    }
}

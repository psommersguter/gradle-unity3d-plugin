/************************************************************
 *  Copyright (c) 2015 Philipp Sommersguter                 *
 *                                                          *
 *  See the file LICENCE.txt for copying permission.        *
 ***********************************************************/
package org.bitbucket.psommersguter.gradle.unity3d.convention;

import org.bitbucket.psommersguter.gradle.unity3d.TaskConfigurationProvider;
import org.bitbucket.psommersguter.gradle.unity3d.Unity3dPlugin;
import org.bitbucket.psommersguter.gradle.unity3d.tasks.DeleteWithIgnore;
import org.bitbucket.psommersguter.gradle.unity3d.tasks.Unity3dExecTask;
import org.gradle.api.NamedDomainObjectContainer;
import org.gradle.api.Project;
import org.gradle.api.Task;
import org.gradle.api.artifacts.Configuration;
import org.gradle.api.logging.Logger;
import org.gradle.api.logging.Logging;
import org.gradle.api.tasks.Copy;
import org.gradle.api.tasks.Delete;
import org.gradle.api.tasks.TaskCollection;

import java.io.File;
import java.nio.file.Paths;

/**
 * <b>Defines a DSL extension to be used in the build script.</b>
 * <p>
 * All public methods can be called from the build script.
 * </p>
 */
public class Unity3dExtension {

    /**
     * The name by which to call the extension from the build script.
     */
    public static final String NAME = "unity3d";

    private static final String TASK_NAME_DEPENDENCIES = "fetchDependencies";
    private static final Logger logger = Logging.getLogger(Unity3dExtension.class);

    /**
     * Whether to use the default clean tasks defined in {@link #createDefaultCleanTasks(Project)}.
     */
    protected boolean useDefaultCleanTasks;

    /**
     * The path to the project created with the Unity3d engine.<p>
     * Has the value defined by {@link #Unity3dExtension(Project)} if not explicitly overridden in the build script.
     */
    protected String projectDir;

    /**
     * The path of the folder containing the dependencies relative to the "Assets" folder of the Unity3d project.<p>
     * Has the value defined by {@link #Unity3dExtension(Project)} if not explicitly overridden in the build script.
     */
    protected String assetDependenciesDir;

    /**
     * The path to the log file created by the Unity3d engine while building the {@link #projectDir}.<p>
     * Has the value defined by {@link #Unity3dExtension(Project)} if not explicitly overridden in the build script.
     */
    private String logFileDir;

    /**
     * The path to the output folder to be created by the Unity3d engine when building the {@link #projectDir}.<p>
     * Has the value defined by {@link #Unity3dExtension(Project)} if not explicitly overridden in the build script.
     */
    private String buildOutputDir;

    /**
     * A container holding all domain objects defined inside the tasks {} of the build.gradle
     */
    private NamedDomainObjectContainer<BuildTaskConvention> buildTaskConventionContainer;

    private Configuration editorConfiguration;
    private Configuration pluginConfiguration;

    public static Unity3dExtension initialize(Project project) {
        return project.getExtensions().create(Unity3dExtension.NAME, Unity3dExtension.class, project);
    }

    /**
     * Defines default values for the extension.
     * @param gradleProject The project used to create the extension
     */
    public Unity3dExtension(Project gradleProject) {
        this.buildOutputDir = gradleProject.getBuildDir().getPath() + File.separator + "unity3d";
        this.logFileDir = buildOutputDir + File.separator + "logs";

        this.useDefaultCleanTasks = true;
        this.projectDir = "src" + File.separator + "main" + File.separator + "unity3d";
        this.assetDependenciesDir = "Dependencies";

        buildTaskConventionContainer = gradleProject.container(BuildTaskConvention.class, name -> {
            String fullProjectDir = gradleProject.getProjectDir().getPath() + File.separator + projectDir;
            return new BuildTaskConvention(name, logFileDir, buildOutputDir,  fullProjectDir);
        });

        //Defines the default configurations to use inside the dependency{} closure.
        editorConfiguration = gradleProject.getConfigurations().create("unity3dEditor");
        pluginConfiguration = gradleProject.getConfigurations().create("unity3dPlugin");

        gradleProject.getExtensions().add("tasks", buildTaskConventionContainer);
    }

    /**
     * Creates the default tasks to copy dependencies.
     * @param gradleProject The project used to create the tasks
     */
    public void createDependencyTasks(Project gradleProject) {
        String assetsDependenciesDir = getAssetDependenciesFolder();

        Task fetchEditorTask = gradleProject.getTasks().create("fetchEditorDependencies", Copy.class,
                TaskConfigurationProvider.getCopy(editorConfiguration, Paths.get(assetsDependenciesDir, "Editor")));

        Task fetchPluginTask = gradleProject.getTasks().create("fetchPluginDependencies", Copy.class,
                TaskConfigurationProvider.getCopy(pluginConfiguration, Paths.get(assetsDependenciesDir, "Plugin")));

        gradleProject.getTasks().create(TASK_NAME_DEPENDENCIES,
                TaskConfigurationProvider.getTaskWithDependencies(
                        "Copies all dependencies into the project.", fetchEditorTask, fetchPluginTask));
    }

    /**
     * Creates tasks from the conventions saved in the {@link Unity3dExtension#buildTaskConventionContainer}
     * @param gradleProject The project used to create the tasks
     */
    public void createBuildTasksFromConventions(Project gradleProject) {
        String editorLocation = gradleProject.getProperties().get(Unity3dPlugin.PROPERTY_EDITOR).toString();
        logger.info("Creating unity build tasks. Using editor location {}", editorLocation);

        for(BuildTaskConvention buildTaskConvention : buildTaskConventionContainer) {
            logger.debug("Creating task \"{}\"", buildTaskConvention.getName());
            gradleProject.getTasks().create(buildTaskConvention.getName(), Unity3dExecTask.class,
                    TaskConfigurationProvider.getConventionBuildTask(buildTaskConvention, editorLocation, TASK_NAME_DEPENDENCIES));
        }
    }

    /**
     * Creates the following tasks:<p>
     * cleanDependencies - <i>Deletes the folder containing the {@link #assetDependenciesDir}</i> <br>
     * cleanOutput - <i>Deletes the build folder</i> <br>
     * cleanProject - <i>Removes everything from the unity project folder except Assets and ProjectSettings</i> <br>
     * clean - <i>Performs all tasks of type {@link Delete} that are found in the project</i> <br>
     * @param gradleProject The project used to create the tasks
     */
    public void createDefaultCleanTasks(Project gradleProject) {
        //check must be here because otherwise the boolean will always have the default value
        if(!useDefaultCleanTasks) {
            return;
        }
        logger.debug("Creating clean tasks. DefaultUnityProjectDirectory={}", projectDir);

        Task cleanDependencies = gradleProject.getTasks().create("cleanDependencies", Delete.class,
                TaskConfigurationProvider.getDelete(getAssetDependenciesFolder(), "Deletes the folder containing the dependencies."));
        cleanDependencies.setGroup(TaskConfigurationProvider.PLUGIN_TASK_GROUP);

        gradleProject.getTasks().create("cleanOutput", Delete.class,
                TaskConfigurationProvider.getDelete(gradleProject.getBuildDir().toString(), "Deletes the 'build' folder."));

        gradleProject.getTasks().create("cleanProject", DeleteWithIgnore.class,
                TaskConfigurationProvider.getDeleteWithIgnore(gradleProject.getProjectDir() + File.separator + projectDir));

        TaskCollection cleanTasks = gradleProject.getTasks().withType(Delete.class);
        gradleProject.getTasks().create("clean", Task.class,
                TaskConfigurationProvider.getTaskWithDependencies(
                        "Cleans the project from generated files and folders.", cleanTasks));
    }

    /**
     * @return The folder containing the dependencies inside the Unity3d project.
     */
    private String getAssetDependenciesFolder(){
        return Paths.get(this.projectDir, "Assets", assetDependenciesDir).toString();
    }

}

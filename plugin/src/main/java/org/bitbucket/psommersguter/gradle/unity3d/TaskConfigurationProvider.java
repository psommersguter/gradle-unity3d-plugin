/************************************************************
 *  Copyright (c) 2015 Philipp Sommersguter                 *
 *                                                          *
 *  See the file LICENCE.txt for copying permission.        *
 ***********************************************************/
package org.bitbucket.psommersguter.gradle.unity3d;

import org.bitbucket.psommersguter.gradle.unity3d.convention.BuildTaskConvention;
import org.bitbucket.psommersguter.gradle.unity3d.tasks.DeleteWithIgnore;
import org.bitbucket.psommersguter.gradle.unity3d.tasks.Unity3dExecTask;
import org.gradle.api.Action;
import org.gradle.api.Task;
import org.gradle.api.artifacts.Configuration;
import org.gradle.api.tasks.Copy;
import org.gradle.api.tasks.Delete;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

/**
 *
 * <b>Provides predefined configurations to use when creating tasks.</b>
 * @see Task
 * @see Action
 */
public class TaskConfigurationProvider {

    /**
     * The name of the group that the tasks of the plugin belong to.
     */
    public static final String PLUGIN_TASK_GROUP = "Unity3d";

    /**
     * Creates the default configuration for the <i>cleanOutput</i> task of the plugin.
     * @param projectBuildDir the path to the build directory of the gradle project
     * @param description the description of the task
     * @return An action that can be used to configure a {@link Task}.
     */
    public static Action<Delete> getDelete(String projectBuildDir, String description) {
        return cleanOutputTask -> {
            cleanOutputTask.setDescription(description);

            cleanOutputTask.delete(projectBuildDir);
        };
    }

    /**
     * Creates the default configuration for the <i>cleanProject</i> task of the plugin.
     * @param unity3dProjectLocation the path to the project created with the Unity3d engine
     * @return An action that can be used to configure a {@link Task}.
     */
    public static Action<DeleteWithIgnore> getDeleteWithIgnore(String unity3dProjectLocation) {
        return cleanProjectTask -> {
            cleanProjectTask.setDescription("Deletes everything in the Unity3d project directory except the folders 'Assets' and 'ProjectSettings'.");

            cleanProjectTask.setTargetDirectory(unity3dProjectLocation);

            cleanProjectTask.ignore("Assets", "ProjectSettings");
        };
    }

    /**
     * Creates the default configuration for a task that copies a group of dependencies into a folder.
     * @param configurationSet the name of the configuration to use
     * @param destination the path to the folder containing the dependencies
     * @return An action that can be used to configure a {@link Task}.
     */
    public static Action<Copy> getCopy(Configuration configurationSet, Path destination) {
        return copy -> {
            copy.setDescription("Copies the dependencies defined as '" + configurationSet.getName() + "' " +
                    "to the 'Assets' folder of the Unity3d project.");
            copy.from(configurationSet);
            copy.into(destination.toFile());
        };
    }

    /**
     * Creates the default configuration for the <i>clean</i> task of the plugin.
     * @param description the description of the task
     * @param taskDependencies the dependencies of the resulting task
     * @return An action that can be used to configure a {@link Task}.
     */
    public static Action<Task> getTaskWithDependencies(String description, Object... taskDependencies) {
        return cleanTask -> {
            cleanTask.setGroup(PLUGIN_TASK_GROUP);
            cleanTask.setDescription(description);

            cleanTask.setDependsOn(Arrays.asList(taskDependencies));
        };
    }

    /**
     * Creates the configuration for a {@link Unity3dExecTask}.
     * @param buildTaskConvention the convention to use
     * @param editor the path to the direction of the Unity3d engine's installation directory
     * @param taskDependencies the dependencies of the resulting task
     * @return An action that can be used to configure a {@link Task}.
     */
    public static Action<Unity3dExecTask> getConventionBuildTask(BuildTaskConvention buildTaskConvention, String editor, Object... taskDependencies) {
        return unityExecTask -> {
            unityExecTask.setGroup(PLUGIN_TASK_GROUP);
            unityExecTask.setDescription(buildTaskConvention.getDescription());

            unityExecTask.setDependsOn(Arrays.asList(taskDependencies));

            unityExecTask.setExecutable(parseUnityExecutable(editor));
            unityExecTask.setProjectPath(buildTaskConvention.getProjectDir());
            unityExecTask.setLogFile(buildTaskConvention.getLogFile());
            unityExecTask.setBuildArgs(buildTaskConvention.getTaskArguments());

            //it seems like there is no other way to define an action to be executed first inside the task, so we do it here
            unityExecTask.doFirst(unityExecTask::createOutputDirs);
        };
    }

    /**
     * Sanitizes an input path to the definite path pointing to the executable of the Unity3d engine.
     * @param editor uncertain input path
     * @return The path to the executable of the Unity3d engine.
     * @throws IllegalArgumentException If the given path could not be sanitized
     */
    public static String parseUnityExecutable(String editor) {
        Path path = Paths.get(editor.toLowerCase());
        StringBuilder generatedPath = new StringBuilder(path.toString());

        if(!path.toString().endsWith(File.separator)) {
            generatedPath.append(File.separator);
        }

        switch (path.getFileName().toString()) {
            case "unity":
                generatedPath.append("Editor").append(File.separator);
            case "editor":
                generatedPath.append("Unity.exe");
                break;
            default:
                throw new IllegalArgumentException("Could not find a valid path to a Unity3d executable " +
                        "from given input \""+editor+"\"!");
        }
        return generatedPath.toString();
    }

}

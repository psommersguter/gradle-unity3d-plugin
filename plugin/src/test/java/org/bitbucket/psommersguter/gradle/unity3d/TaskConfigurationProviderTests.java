package org.bitbucket.psommersguter.gradle.unity3d;

import org.junit.Assert;
import org.junit.Test;

import java.nio.file.Paths;

/**
 * Contains small test snippets that don't need to be tested in a full gradle project environment
 */
public class TaskConfigurationProviderTests extends TaskConfigurationProvider {
    //Extending the TaskConfigurationProvider class ensures:
    // 1. 100% code coverage (implicit constructor call)
    // 2. That the class is extendable if someone wants to add helper methods

    @Test(expected = IllegalArgumentException.class)
    public void testSanitizeUnityLocation() throws Throwable {

        String expectedLowerCase = "\\"+Paths.get("mock", "unity", "editor", "unity.exe").toString().toLowerCase();

        String input1 = "/mock/unity/";
        String input2 = "/mock/unity/editor";
        String input3 = "/mock/unity\\editor/unity.exe";
        String input4 = "/mock/unity/xx";
        String inputIllegal = "/illegal";

        Assert.assertEquals(expectedLowerCase, parseUnityExecutable(input1).toLowerCase());
        Assert.assertEquals(expectedLowerCase, parseUnityExecutable(input2).toLowerCase());
        Assert.assertEquals(expectedLowerCase, parseUnityExecutable(input3).toLowerCase());
        Assert.assertEquals(expectedLowerCase, parseUnityExecutable(input4).toLowerCase());

        parseUnityExecutable(inputIllegal);
    }


}

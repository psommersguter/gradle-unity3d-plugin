/************************************************************
 *  Copyright (c) 2015 Philipp Sommersguter                 *
 *                                                          *
 *  See the file LICENCE.txt for copying permission.        *
 ***********************************************************/
package org.bitbucket.psommersguter.gradle.unity3d.convention;

/**
 * Defines the standard build types that are supported out of the box.
 */
@SuppressWarnings("unused")
public enum BuildType {
    LINUX_32BIT("-buildLinux32Player", ""),
    LINUX_64BIT("-buildLinux64Player", ""),
    LINUX_UNIVERSAL("-buildLinuxUniversalPlayer", ""),

    OSX_32BIT("-buildOSXPlayer",   ".app"),
    OSX_64BIT("-buildOSX64Player", ".app"),
    OSX_UNIVERSAL("-buildOSXUniversalPlayer", ".app"),

    WINDOWS_32BIT("-buildWindowsPlayer",   ".exe"),
    WINDOWS_64BIT("-buildWindows64Player", ".exe"),

    BROWSER_WEBPLAYER("-buildWebPlayer", ""),
    BROWSER_WEBPLAYER_STREAMED("-buildWebPlayerStreamed", ""),

    CUSTOM("", "");

    private final String argument;
    private final String extension;

    BuildType(String argument, String extension) {
        this.argument = argument;
        this.extension = extension;
    }

    public String getArgument() {
        return argument;
    }

    public String getExtension() {
        return extension;
    }
}

/*
    The MIT License

    Copyright (c) 2011-, Jerome Lacoste

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
*/
package org.jenkinsci.plugins.unity3d.io;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.util.concurrent.Callable;

/**
 * A Callable that allows piping the output of a file into.
 * <p>
 * Useful when the file is been modified and one needs to loop.
 * <p>
 * Note that you can interrupt this task to cancel it.
 *
 * @author Jerome Lacoste
 */
public class PipeFileAfterModificationAction implements Callable<Long> {
    private final String path;
    private final OutputStream out;
    private final boolean closeOut;
    private final int waitBetweenCopyLoops;

    public PipeFileAfterModificationAction(String path, OutputStream out, boolean closeOut) {
        this.path = path;
        if (out == null) {
            throw new NullPointerException("out is null");
        }
        this.out = out;
        this.closeOut = closeOut;
        waitBetweenCopyLoops = 50;
    }

    public PipeFileAfterModificationAction(String path, OutputStream out) {
        this(path, out, false);
    }

    /**
     * Wait until the file has been modified and then copy its contents into the output, looping repeatedly as the file is been modified.
     * @return the number of bytes copied
     * @throws IOException
     */
    public Long call() throws IOException {
        File file = new DetectFileCreatedOrModifiedAction(path).call();
        long pos = 0;
        if (file != null) {
            RandomAccessFile raf = null;
            try {
                while (true) {
                    // I once experienced a FileNotFoundException here, let's see if this happens again
                    raf = new RandomAccessFile(path, "r");
                    pos = continueCopyingFrom(raf, pos);
                    raf.close();

                    synchronized (this) {
                        try {
                            wait(waitBetweenCopyLoops);
                        } catch (InterruptedException e) {
                            break;
                        }
                    }
                }
            } catch (Throwable t) {
                t.printStackTrace(new PrintStream(out));
            } finally {
                if (raf != null) {
                    try {
                        pos = raf.getFilePointer();
                    } catch (IOException ignore) {
                    }
                    try {
                        raf.close();
                    } catch (IOException ignore) {
                    }
                }
                if (closeOut)
                    out.close();
            }
        }
        return pos;
    }

    private long continueCopyingFrom(RandomAccessFile raf, long from) throws IOException {
        raf.seek(from);

        byte[] buf = new byte[8192];
        int len;
        while ((len = raf.read(buf)) > 0)
            out.write(buf, 0, len);

        return raf.getFilePointer();
    }

}

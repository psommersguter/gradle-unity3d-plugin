/************************************************************
 *  Copyright (c) 2015 Philipp Sommersguter                 *
 *                                                          *
 *  See the file LICENCE.txt for copying permission.        *
 ***********************************************************/
package org.bitbucket.psommersguter.gradle.unity3d.convention;

import org.bitbucket.psommersguter.gradle.unity3d.Unity3dPlugin;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * <b>Provides data to use when creating build tasks with {@link Unity3dExtension#createBuildTasksFromConventions(org.gradle.api.Project)}.</b>
 */
public class BuildTaskConvention {

    private final String name;
    private final String projectDir;
    private final String logFile;
    private final String taskOutputPath;

    private String description;
    private String executeMethod;
    private String buildName;
    private BuildType buildType;
    private List<String> buildArgs;
    /**
     * The path to the build output as expected by the Unity3d <a href="http://docs.unity3d.com/Manual/CommandLineArguments.html">command line arguments</a>.
     * The specified value will be added to {@link #taskOutputPath}.
     */
    private String buildPath;

    public BuildTaskConvention(String name, String logFileDir, String buildOutputDir, String projectDir) {
        this.name = name;
        this.projectDir = projectDir;
        this.logFile = logFileDir + File.separator + name + ".log";
        this.taskOutputPath = buildOutputDir + File.separator + name;

        this.description = "";
        this.executeMethod = null;
        this.buildPath = null;
        this.buildType = BuildType.CUSTOM;
        this.buildName = "game";
        this.buildArgs = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    /**
     * Builds list of arguments to pass to a {@link org.bitbucket.psommersguter.gradle.unity3d.tasks.Unity3dExecTask}
     * @return A list of command line arguments
     */
    public List<String> getTaskArguments() {
        if(executeMethod != null) {
            this.buildArgs.add(Unity3dPlugin.PARAM_EXECUTE_METHOD);
            this.buildArgs.add(executeMethod);
        }

        if(buildType != BuildType.CUSTOM) {
            this.buildArgs.add(buildType.getArgument());
            this.buildArgs.add(getOutputArgument());
        }
        return buildArgs;
    }

    public String getLogFile() {
        return logFile;
    }

    public String getProjectDir() {
        return projectDir;
    }

    /**
     * Builds a String to be passed as the <b>&lt;pathname&gt;</b> argument as expected by
     * the Unity3d <a href="http://docs.unity3d.com/Manual/CommandLineArguments.html">command line arguments</a>.
     * @return The value do be used as a command line argument
     */
    public String getOutputArgument() {
        String path;
        if(this.buildPath == null) {
            path = File.separator + buildName + buildType.getExtension();
        }else {
            path = File.separator + this.buildPath + buildName;
        }
        return this.taskOutputPath + path;
    }
}

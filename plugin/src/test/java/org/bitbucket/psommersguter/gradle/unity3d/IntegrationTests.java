/************************************************************
 *  Copyright (c) 2015 Philipp Sommersguter                 *
 *                                                          *
 *  See the file LICENCE.txt for copying permission.        *
 ***********************************************************/
package org.bitbucket.psommersguter.gradle.unity3d;

import org.gradle.tooling.ProjectConnection;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

/**
 * <b>Performs integration tests by actually running gradle builds of projects
 * defined in the <i>src/test/resources</i> directory.</b>
 */
public class IntegrationTests extends IntegrationTestBase {

    private final ByteArrayOutputStream systemOutCapture = new ByteArrayOutputStream();
    private PrintStream savedSystemOutStream;

    @Test
    public void testApplyPluginById() throws Throwable {
        ProjectConnection connection = openProjectConnection("apply-by-id");
        launchTasks(connection, "tasks");
    }

    @Test
    public void testDefaultTasksDisabled() throws Throwable {
        ProjectConnection connection = openProjectConnection("default-tasks-disabled");
        ByteArrayOutputStream outputStream = launchTasks(connection, "tasks");

        String output = outputStream.toString();
        List<String> tasksMustNotExist = Arrays.asList("clean", "cleanProject", "cleanOutput");

        tasksMustNotExist.forEach(taskName -> Assert.assertFalse("Found task \"" + taskName + "\"", output.contains(taskName)));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testExceptionWhenEditorNotSet() throws Throwable {
        ProjectConnection connection = openProjectConnection("editor-not-set");
        launchTasks(connection, "tasks");
    }

    @Test
    public void testCleanBuildOutput() throws Throwable {
        String testProjectDirectory = "clean-build-output";

        ProjectConnection connection = openProjectConnection(testProjectDirectory);
        launchTasks(connection, "cleanOutput");

        assertBuildDirDeleted(testProjectDirectory);
    }

    @Test
    public void testCleanUnityProject() throws Throwable {
        String testProjectDirectory = "clean-unity-project";

        ProjectConnection connection = openProjectConnection(testProjectDirectory);
        launchTasks(connection, "cleanProject");

        assertUnityProjectClean(testProjectDirectory);
    }


    @Test
    public void testCleanWholeProject() throws Throwable {
        String testProjectDirectory = "clean-whole-project";

        ProjectConnection connection = openProjectConnection(testProjectDirectory);
        launchTasks(connection, "clean");

        assertBuildDirDeleted(testProjectDirectory);
        assertUnityProjectClean(testProjectDirectory);
    }

    @Test
    public void testChangedProjectDir() throws Throwable {
        String testProjectDirectory = "build-changed-projectdir";

        ProjectConnection connection = openProjectConnection(testProjectDirectory);
        launchTasks(connection, asArray(getEditorArgument()), "build");

        String buildDir = getAsTestProjectPath(testProjectDirectory)+File.separator+"build"+File.separator+"unity3d";
        Path[] pathsThatMustExist = new Path[] {
                Paths.get(buildDir, "logs", "build.log"),
                Paths.get(buildDir, "build", "game.exe")
        };

        Arrays.asList(pathsThatMustExist).forEach(path ->
                Assert.assertTrue(path.toString()+" does not exist!", path.toFile().exists()));
    }

    @Test
    public void testBuildTypeCustom() throws Throwable {
        String testProjectDirectory = "build-type-custom";

        ProjectConnection connection = openProjectConnection(testProjectDirectory);
        launchTasks(connection, asArray(getEditorArgument()), "build");

        String buildDir = getAsTestProjectPath(testProjectDirectory)+File.separator+"build"+File.separator+"unity3d";
        Path[] pathsThatMustExist = new Path[] {
                Paths.get(buildDir, "logs", "build.log"),
                Paths.get(buildDir, "build", "asdf", "fdsa", "great")
        };

        Arrays.asList(pathsThatMustExist).forEach(path ->
                Assert.assertTrue(path.toString()+" does not exist!", path.toFile().exists()));
    }

    @Test
    public void testBuildWithArguments() throws Throwable {
        String testProjectDirectory = "build-with-arguments";

        ProjectConnection connection = openProjectConnection(testProjectDirectory);
        launchTasks(connection, asArray(getEditorArgument()), "build");

        String buildDir = getAsTestProjectPath(testProjectDirectory)+File.separator+"build"+File.separator+"unity3d";
        Path[] pathsThatMustExist = new Path[] {
                Paths.get(buildDir, "logs", "build.log"),
                Paths.get(buildDir, "build", "game")
        };

        Arrays.asList(pathsThatMustExist).forEach(path ->
                Assert.assertTrue(path.toString()+" does not exist!", path.toFile().exists()));
    }

    @Test
    public void testBuildWithUnityLogProperty() throws Throwable {
        prepareStreamCapture();
        String testProjectDirectory = "default-build-unity-project";
        String[] gradleLaunchArguments = asArray(getEditorArgument(), "-PpipeUnityLog");

        ProjectConnection connection = openProjectConnection(testProjectDirectory);
        launchTasks(connection, gradleLaunchArguments, "cleanOutput", "build");

        Assert.assertTrue(systemOutCapture.toString().contains("BATCHMODE ARGUMENTS:"));
        Assert.assertTrue(systemOutCapture.toString().contains("Exiting batchmode successfully now!"));

        String buildDir = getAsTestProjectPath(testProjectDirectory)+File.separator+"build"+File.separator+"unity3d";
        Path[] pathsThatMustExist = new Path[] {
                Paths.get(buildDir, "logs", "build.log"),
                Paths.get(buildDir, "build", "game.exe")
        };

        Arrays.asList(pathsThatMustExist).forEach(path ->
                Assert.assertTrue(path.toString()+" does not exist!", path.toFile().exists()));
        cleanUpStreamCapture();
    }

    @Test
    public void testBuildWithDebugFlag() throws Throwable {
        prepareStreamCapture();
        String testProjectDirectory = "default-build-unity-project";
        String[] gradleLaunchArguments = asArray(getEditorArgument(), "-d");

        ProjectConnection connection = openProjectConnection(testProjectDirectory);
        launchTasks(connection, gradleLaunchArguments, "cleanOutput", "build");

        Assert.assertTrue(systemOutCapture.toString().contains("BATCHMODE ARGUMENTS:"));
        Assert.assertTrue(systemOutCapture.toString().contains("Exiting batchmode successfully now!"));

        String buildDir = getAsTestProjectPath(testProjectDirectory)+File.separator+"build"+File.separator+"unity3d";
        Path[] pathsThatMustExist = new Path[] {
                Paths.get(buildDir, "logs", "build.log"),
                Paths.get(buildDir, "build", "game.exe")
        };

        Arrays.asList(pathsThatMustExist).forEach(path ->
                Assert.assertTrue(path.toString()+" does not exist!", path.toFile().exists()));
        cleanUpStreamCapture();
    }

    @Test
    public void testDefaultBuild() throws Throwable {
        String testProjectDirectory = "default-build-unity-project";

        ProjectConnection connection = openProjectConnection(testProjectDirectory);
        launchTasks(connection, asArray(getEditorArgument()), "build");

        String buildDir = getAsTestProjectPath(testProjectDirectory)+File.separator+"build"+File.separator+"unity3d";
        Path[] pathsThatMustExist = new Path[] {
                Paths.get(buildDir, "logs", "build.log"),
                Paths.get(buildDir, "build")
        };

        Arrays.asList(pathsThatMustExist).forEach(path ->
                Assert.assertTrue(path.toString()+" does not exist!", path.toFile().exists()));
    }

    @Test
    public void testMultiTaskBuild() throws Throwable {
        String testProjectDirectory = "multi-task-build";

        ProjectConnection connection = openProjectConnection(testProjectDirectory);
        ByteArrayOutputStream byteArrayOutputStream = launchTasks(connection, asArray(getEditorArgument()), "buildWindows", "buildMac");

        Assert.assertTrue(byteArrayOutputStream.toString().contains(":buildWindows"));
        Assert.assertTrue(byteArrayOutputStream.toString().contains(":buildMac"));

        String buildDir = getAsTestProjectPath(testProjectDirectory)+File.separator+"build"+File.separator+"unity3d";
        Path[] pathsThatMustExist = new Path[] {
                Paths.get(buildDir, "logs", "buildWindows.log"),
                Paths.get(buildDir, "logs", "buildMac.log"),
                Paths.get(buildDir, "buildWindows", "game.exe"),
                Paths.get(buildDir, "buildMac", "game.app")
        };

        Arrays.asList(pathsThatMustExist).forEach(path ->
                Assert.assertTrue(path.toString()+" does not exist!", path.toFile().exists()));
    }

    @Test
    public void testRunMethod() throws Throwable {
        String testProjectDirectory = "task-run-method";

        ProjectConnection connection = openProjectConnection(testProjectDirectory);
        launchTasks(connection, asArray(getEditorArgument()), "runMethod");

        String buildDir = getAsTestProjectPath(testProjectDirectory)+File.separator+"build"+File.separator+"unity3d";

        Path logFilePath = Paths.get(buildDir, "logs", "runMethod.log");
        Assert.assertTrue(logFilePath.toString() + " does not exist!", logFilePath.toFile().exists());

        String logFileContent = new String(Files.readAllBytes(logFilePath));
        Assert.assertTrue(logFileContent.contains("Asdf.Build.Method() called"));
    }

    //region assertion helper methods

    private void assertBuildDirDeleted(String testProjectDirectory) {
        File buildDir = new File(getAsTestProjectPath(testProjectDirectory)+File.separator+"build");
        Assert.assertFalse(buildDir.exists());
    }

    private void assertUnityProjectClean(String testProjectDirectory) {
        String unityProjectDirectory = getAsTestProjectPath(testProjectDirectory)
                + File.separator + "src" + File.separator + "main" + File.separator + "unity3d";
        File[] unityProjectContent = new File(unityProjectDirectory).listFiles();

        Assert.assertNotNull(unityProjectContent);
        Assert.assertEquals(2, unityProjectContent.length);

        for (File file : unityProjectContent) {
            if(!file.getName().equals("Assets") && !file.getName().equals("ProjectSettings")) {
                Assert.fail("\""+file.getName()+"\" is not an essential unity project directory!");
            }
        }
    }

    //endregion

    //region capture System.out

    private void prepareStreamCapture() {
        savedSystemOutStream = System.out;
        System.setOut(new PrintStream(systemOutCapture));
    }


    private void cleanUpStreamCapture() {
        System.setOut(savedSystemOutStream);
        savedSystemOutStream = null;
    }

    //endregion
}

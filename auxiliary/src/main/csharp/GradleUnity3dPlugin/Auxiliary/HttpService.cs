﻿using System.IO;
using System.Net;
using System.Text;
using System.Threading;

namespace GradleUnity3dPlugin.Auxiliary
{
    public class HttpService
    {
        private const string ACCEPT_FROM = "http://*:9173";
        private const string ACCEPT_PATH = "/unity/executemethod/";
        private const string LISTENER_PREFIX = ACCEPT_FROM + ACCEPT_PATH;

        private HttpListener httpListener;
        public string RequestInvokeTarget { get; set; }

        public HttpService()
        {
            httpListener = new HttpListener();
            httpListener.Prefixes.Add(LISTENER_PREFIX);
        }

        public void StartService()
        {
            if (!httpListener.IsListening)
            {
                httpListener.Start();
            }

            Thread handleThread = new Thread(new ParameterizedThreadStart(HandleRequest));
            handleThread.Start(httpListener);
        }

        private void HandleRequest(object listener)
        {
            HttpListener httpListener = (HttpListener) listener;
            while (true)
            {
                HttpListenerContext context = httpListener.GetContext();
                HttpListenerRequest request = context.Request;

                string invokeTarget = request.RawUrl.Replace(ACCEPT_PATH, "");
                RequestInvokeTarget = invokeTarget;
                
                BuildResponse(context.Response);
            }
        }

        private void BuildResponse(HttpListenerResponse response)
        {
            response.StatusCode = 200;

            string responseString = "<HTML><BODY> Status: OK!</BODY></HTML>";
            byte[] buffer = Encoding.UTF8.GetBytes(responseString);
            // Get a response stream and write the response to it.
            response.ContentLength64 = buffer.Length;
            Stream output = response.OutputStream;
            output.Write(buffer, 0, buffer.Length);
            // You must close the output stream.
            output.Close();
        }
    }
}
